console.log("hello")

// mock database

let posts = []

// post ID

let count = 1

// add post
// this will trigger an event that will add a new post in our mock database upon clicking the create button

document.querySelector("#form-add-post").addEventListener("submit", (event) =>{
	event.preventDefault();
	posts.push({
		id : count,
		title:document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	})
	count++
	showPost(posts);
	alert("successfully added")
	
	
})

// show post
const showPost = (post) => {

	// this is a variable that will contain all the post.

	let postEntries = ""
	posts.forEach((post)=> {
		postEntries+=`<div id="post-${post.id}">
					<h3 id="post-title-${post.id}">${post.title}</h3>
					<p id="post-body-${post.id}">${post.body}</p>
					<button onclick="editPost('${post.id}')">Edit</button>
					<button onclick="deletePost('${post.id}')">Delete</button>
		</div`
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries


}



// edit post
// this will trigger an event that will update a certain post upon clicking the edit button

/*
    Transfer the id, title and body of each post entry to the second form when the edit button has been clicked:
        -  the id of the div will be copied to the hidden input field of Edit Post form
        - the title of the div will be copied to the title input field of Edit Post form
        - the body of the div will be copied to the body input field of Edit Post form
*/

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
}

document.querySelector("#form-edit-post").addEventListener ("submit", (e) => {
	e.preventDefault();

for(let i = 0; i < posts.length; i++ ) {
	if (posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
		posts[i].title = document.querySelector("#txt-edit-title").value;
		posts[i].body = document.querySelector("#txt-edit-body").value;
		showPost(posts);
		alert("successfully updated");
		break;

	};
}
});


// activity delete post



const deletePost = (id) => {

	let indexOfPost = posts.findIndex( post => post.id == id )
	posts.splice(indexOfPost, 1)
	showPost(posts)
	alert("successfully deleted")

	};



